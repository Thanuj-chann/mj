<?php
session_start();
if (!isset($_SESSION['user_id']) || !isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

$polygon_api_key = "pbeVXEnzi5SMhUsbMI7usDm4ykfiCill";
$alpaca_api_key = 'PKKW8ZAJGUYHXPC2LFO0';
$alpaca_api_secret = 'CrP2tkYQf0svdsbhgw0YPIys3UQZ6niQ6hPaPqND';

function make_api_request($url, $headers = []) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if ($headers) curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $error ? ['error' => $error] : json_decode($response, true);
}
$stock_url = "https://api.polygon.io/v3/reference/tickers/AAPL?apiKey=$polygon_api_key";
$stock_data = make_api_request($stock_url);
$market_status_url = "https://api.polygon.io/v1/marketstatus/now?apiKey=$polygon_api_key";
$market_data = make_api_request($market_status_url);
$asset_info = null;
$error = null;
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['symbol'])) {
    $symbol = urlencode($_POST['symbol']);
    $alpaca_url = "https://paper-api.alpaca.markets/v2/assets/$symbol";
    $headers = [
        "APCA-API-KEY-ID: $alpaca_api_key",
        "APCA-API-SECRET-KEY: $alpaca_api_secret"
    ];
    $asset_info = make_api_request($alpaca_url, $headers);
    $error = $asset_info['error'] ?? null;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stock and Market Information</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container mt-5">
    <h1 class="text-center">Market and Stock Information</h1>
    <?php if ($stock_data && isset($stock_data['results'])): ?>
        <div class="card my-4">
            <div class="card-header bg-primary text-white">
                <h2><?php echo $stock_data['results']['name']; ?> (<?php echo $stock_data['results']['ticker']; ?>)</h2>
            </div>
            <div class="card-body">
                <p><strong>Market Cap:</strong> $<?php echo number_format($stock_data['results']['market_cap'], 2); ?></p>
                <p><strong>Industry:</strong> <?php echo $stock_data['results']['sic_description']; ?></p>
                <p><strong>Website:</strong> <a href="<?php echo $stock_data['results']['homepage_url']; ?>" target="_blank"><?php echo $stock_data['results']['homepage_url']; ?></a></p>
                <img src="<?php echo $stock_data['results']['branding']['logo_url']; ?>" alt="Company Logo" class="img-fluid mt-3">
            </div>
        </div>
    <?php endif; ?>

    <?php if ($market_data): ?>
        <div class="card my-4">
            <div class="card-body">
                <h5>Market: <?php echo ucfirst($market_data['market']); ?></h5>
                <p><strong>After Hours:</strong> <?php echo $market_data['afterHours'] ? 'Open' : 'Closed'; ?></p>
                <p><strong>Server Time:</strong> <?php echo $market_data['serverTime']; ?></p>
            </div>
        </div>
    <?php endif; ?>

    <h2>Alpaca Asset Information</h2>
    <form method="post">
        <label for="symbol">Enter Asset Symbol:</label>
        <input type="text" id="symbol" name="symbol" required>
        <input type="submit" value="Get Asset Info">
    </form>

    <?php if ($error): ?>
        <p class="text-danger"><?php echo htmlspecialchars($error); ?></p>
    <?php endif; ?>

    <?php if ($asset_info && !isset($error)): ?>
        <div class="data-container">
            <h3>Asset Information for <?php echo htmlspecialchars($asset_info['symbol']); ?></h3>
            <table class="table table-bordered">
                <tr><th>Asset ID</th><td><?php echo htmlspecialchars($asset_info['id'] ?? 'N/A'); ?></td></tr>
                <tr><th>Name</th><td><?php echo htmlspecialchars($asset_info['name'] ?? 'N/A'); ?></td></tr>
                <tr><th>Status</th><td><?php echo htmlspecialchars($asset_info['status'] ?? 'N/A'); ?></td></tr>
                <tr><th>Tradable</th><td><?php echo $asset_info['tradable'] ? 'Yes' : 'No'; ?></td></tr>
            </table>
        </div>
    <?php endif; ?>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
