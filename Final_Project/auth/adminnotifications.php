<?php
session_start();
include '../db.php';

// Check if the user is an admin
if (!isset($_SESSION['user_id']) || $_SESSION['role'] !== 'admin') {
    header("Location: login.php");
    exit();
}

// Fetch all notifications
$query = "SELECT n.activity_type, n.description, n.created_at, u.username 
          FROM notifications n 
          JOIN auth_user u ON n.user_id = u.id 
          ORDER BY n.created_at DESC";
$notifications = $conn->query($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All User Activities</title>
</head>
<body>
    <h2>All User Activity Notifications</h2>

    <?php if ($notifications->num_rows > 0): ?>
        <table border="1" cellpadding="10">
            <tr>
                <th>Username</th>
                <th>Activity Type</th>
                <th>Description</th>
                <th>Timestamp</th>
            </tr>
            <?php while ($notification = $notifications->fetch_assoc()): ?>
                <tr>
                    <td><?php echo htmlspecialchars($notification['username']); ?></td>
                    <td><?php echo htmlspecialchars($notification['activity_type']); ?></td>
                    <td><?php echo htmlspecialchars($notification['description']); ?></td>
                    <td><?php echo $notification['created_at']; ?></td>
                </tr>
            <?php endwhile; ?>
        </table>
    <?php else: ?>
        <p>No recent activity found.</p>
    <?php endif; ?>

    <p><a href="admin_dashboard.php">Back to Admin Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
