<?php
require_once('../vendor/autoload.php');

function getAlpacaAssets() {
    $client = new \GuzzleHttp\Client();

    try {
        $response = $client->request('GET', 'https://paper-api.alpaca.markets/v2/assets', [
            'headers' => [
                'APCA-API-KEY-ID' => 'PKKW8ZAJGUYHXPC2LFO0',
                'APCA-API-SECRET-KEY' => 'CrP2tkYQf0svdsbhgw0YPIys3UQZ6niQ6hPaPqND',
                'accept' => 'application/json',
            ],
        ]);

        return $response->getBody();
    } catch (Exception $e) {
        return json_encode(['error' => $e->getMessage()]);
    }
}
if (isset($_GET['fetch'])) {
    header('Content-Type: application/json');
    echo getAlpacaAssets();
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alpaca Assets</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Alpaca Assets</h1>
    <button id="fetchButton">Fetch Alpaca Assets</button>
    <div id="tableContainer"></div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#fetchButton').click(function() {
                $.ajax({
                    url: '?fetch=true',
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        var tableHtml = '<table><tr><th>ID</th><th>Class</th><th>Exchange</th><th>Symbol</th><th>Name</th><th>Status</th><th>Tradable</th><th>Marginable</th><th>Shortable</th><th>Easy to Borrow</th><th>Fractionable</th></tr>';
                        
                        // Limit the data to 20 assets
                        var limitedData = data.slice(0, 20);

                        $.each(limitedData, function(i, asset) {
                            tableHtml += '<tr>';
                            tableHtml += '<td>' + (asset.id || '') + '</td>';
                            tableHtml += '<td>' + (asset.class || '') + '</td>';
                            tableHtml += '<td>' + (asset.exchange || '') + '</td>';
                            tableHtml += '<td>' + (asset.symbol || '') + '</td>';
                            tableHtml += '<td>' + (asset.name || '') + '</td>';
                            tableHtml += '<td>' + (asset.status || '') + '</td>';
                            tableHtml += '<td>' + (asset.tradable ? 'Yes' : 'No') + '</td>';
                            tableHtml += '<td>' + (asset.marginable ? 'Yes' : 'No') + '</td>';
                            tableHtml += '<td>' + (asset.shortable ? 'Yes' : 'No') + '</td>';
                            tableHtml += '<td>' + (asset.easy_to_borrow ? 'Yes' : 'No') + '</td>';
                            tableHtml += '<td>' + (asset.fractionable ? 'Yes' : 'No') + '</td>';
                            tableHtml += '</tr>';
                        });
                        
                        tableHtml += '</table>';
                        $('#tableContainer').html(tableHtml);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('#tableContainer').html('Error fetching data: ' + textStatus);
                    }
                });
            });
        });
    </script>
</body>
</html>