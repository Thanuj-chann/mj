<?php
session_start();
include '../db.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$error_message = '';
$success_message = '';

if (isset($_POST['create_post'])) {
    $content = $_POST['content'];
    $user_id = $_SESSION['user_id'];

    if (empty($content)) {
        $error_message = "Post content cannot be empty.";
    } else {
        $stmt = $conn->prepare("INSERT INTO posts (user_id, content) VALUES (?, ?)");
        if ($stmt === false) {
            die("Error preparing statement: " . $conn->error);
        }

        $stmt->bind_param("is", $user_id, $content);
        if ($stmt->execute()) {
            $success_message = "Post created successfully!";
        } else {
            $error_message = "Error creating post: " . $stmt->error;
        }
        $stmt->close();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Post</title>
</head>
<body>
    <h2>Create a New Post</h2>

    <?php
    if (!empty($error_message)) {
        echo "<p style='color: red;'>$error_message</p>";
    }
    if (!empty($success_message)) {
        echo "<p style='color: green;'>$success_message</p>";
    }
    ?>

    <form action="create_post.php" method="POST">
        <label for="content">Post Content:</label><br>
        <textarea id="content" name="content" rows="4" cols="50" required></textarea><br><br>
        
        <button type="submit" name="create_post">Create Post</button>
    </form>

    <p><a href="welcome.php">Back to Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
