<?php
session_start();

if (!isset($_SESSION['user_id']) || !isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

$api_key = 'PKKW8ZAJGUYHXPC2LFO0';
$api_secret = 'CrP2tkYQf0svdsbhgw0YPIys3UQZ6niQ6hPaPqND';

function make_api_request($endpoint, $api_key, $api_secret) {
    $base_url = 'https://paper-api.alpaca.markets';
    $url = $base_url . $endpoint;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'APCA-API-KEY-ID: ' . $api_key,
        'APCA-API-SECRET-KEY: ' . $api_secret
    ]);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    
    if ($error) {
        return ['error' => 'cURL Error: ' . $error];
    }
    
    $data = json_decode($response, true);
    
    if ($status_code != 200) {
        return ['error' => 'API Error: ' . ($data['message'] ?? 'Unknown error') . ' (Status code: ' . $status_code . ')'];
    }
    
    return $data;
}

function get_asset($symbol, $api_key, $api_secret) {
    $endpoint = "/v2/assets/" . urlencode($symbol);
    return make_api_request($endpoint, $api_key, $api_secret);
}

$asset_info = null;
$error = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $symbol = $_POST['symbol'] ?? '';
    if (!empty($symbol)) {
        $asset_info = get_asset($symbol, $api_key, $api_secret);
        if (isset($asset_info['error'])) {
            $error = $asset_info['error'];
            $asset_info = null;
        }
    } else {
        $error = "Please enter a symbol.";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alpaca Asset Information</title>
    <style>
        body { font-family: Arial, sans-serif; line-height: 1.6; padding: 20px; }
        h1, h2 { color: #333; }
        .data-container { background-color: #f4f4f4; padding: 15px; border-radius: 5px; margin-top: 20px; }
        table { width: 100%; border-collapse: collapse; }
        th, td { padding: 10px; text-align: left; border-bottom: 1px solid #ddd; }
        th { background-color: #f2f2f2; }
        .error { color: red; }
        form { margin-bottom: 20px; }
        input[type="text"], input[type="submit"] { padding: 5px; margin-right: 10px; }
    </style>
</head>
<body>
    <h1>Alpaca Asset Information</h1>

    <form method="post">
        <label for="symbol">Enter Asset Symbol:</label>
        <input type="text" id="symbol" name="symbol" required>
        <input type="submit" value="Get Asset Info">
    </form>

    <?php if ($error): ?>
        <p class="error"><?php echo htmlspecialchars($error); ?></p>
    <?php endif; ?>

    <?php if ($asset_info): ?>
        <div class="data-container">
            <h2>Asset Information for <?php echo htmlspecialchars($asset_info['symbol']); ?></h2>
            <table>
                <tr>
                    <th>Asset ID</th>
                    <td><?php echo htmlspecialchars($asset_info['id'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Class</th>
                    <td><?php echo htmlspecialchars($asset_info['class'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Exchange</th>
                    <td><?php echo htmlspecialchars($asset_info['exchange'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Symbol</th>
                    <td><?php echo htmlspecialchars($asset_info['symbol'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td><?php echo htmlspecialchars($asset_info['name'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><?php echo htmlspecialchars($asset_info['status'] ?? 'N/A'); ?></td>
                </tr>
                <tr>
                    <th>Tradable</th>
                    <td><?php echo $asset_info['tradable'] ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Marginable</th>
                    <td><?php echo $asset_info['marginable'] ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Shortable</th>
                    <td><?php echo $asset_info['shortable'] ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Easy to Borrow</th>
                    <td><?php echo $asset_info['easy_to_borrow'] ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Fractionable</th>
                    <td><?php echo $asset_info['fractionable'] ? 'Yes' : 'No'; ?></td>
                </tr>
            </table>
        </div>
    <?php endif; ?>

    <a href="welcome.php">Back to Welcome Page</a>
</body>
</html>