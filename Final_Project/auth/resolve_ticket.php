<?php
session_start();
include '../db.php';

if (!isset($_SESSION['user_id']) || $_SESSION['role'] !== 'admin') {
    header("Location: login.php");
    exit();
}

if (isset($_POST['resolve_ticket'])) {
    $ticket_id = $_POST['ticket_id'];
    $admin_response = $_POST['admin_response'];

    $stmt = $conn->prepare("UPDATE tickets SET admin_response = ?, status = 'Resolved', resolved_at = NOW() WHERE id = ?");
    $stmt->bind_param("si", $admin_response, $ticket_id);

    if ($stmt->execute()) {
        echo "Ticket resolved successfully.";
    } else {
        echo "Error: " . $stmt->error;
    }

    $stmt->close();
    header("Location: admin_dashboard.php");
    exit();
}

if (isset($conn)) {
    $conn->close();
}
?>
