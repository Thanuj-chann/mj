<?php
session_start();
include '../db.php';

// Check if the user is logged in and is an admin
if (!isset($_SESSION['user_id']) || $_SESSION['role'] !== 'admin') {
    header("Location: login.php");
    exit();
}

// Fetch all users
$user_query = "SELECT id, username, first_name, last_name, email, role, created_at FROM auth_user";
$user_result = $conn->query($user_query);

// Fetch all tickets
$ticket_query = "SELECT t.id, t.subject, t.message, t.status, t.created_at, u.username 
                 FROM tickets t 
                 JOIN auth_user u ON t.user_id = u.id
                 ORDER BY t.status DESC, t.created_at DESC";
$ticket_result = $conn->query($ticket_query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
</head>
<body>
    <h1>Admin Dashboard</h1>
    <h2>Welcome, <?php echo $_SESSION['username']; ?>!</h2>

    <section>
        <h3>User Management</h3>
        <table border="1" cellpadding="10">
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Created At</th>
            </tr>
            <?php while ($user = $user_result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo htmlspecialchars($user['username']); ?></td>
                    <td><?php echo htmlspecialchars($user['first_name']); ?></td>
                    <td><?php echo htmlspecialchars($user['last_name']); ?></td>
                    <td><?php echo htmlspecialchars($user['email']); ?></td>
                    <td><?php echo htmlspecialchars($user['role']); ?></td>
                    <td><?php echo $user['created_at']; ?></td>
                </tr>
            <?php endwhile; ?>
        </table>
    </section>

    <section>
        <h3>Support Tickets</h3>
        <table border="1" cellpadding="10">
            <tr>
                <th>Ticket ID</th>
                <th>Subject</th>
                <th>Message</th>
                <th>User</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Actions</th>
            </tr>
            <?php while ($ticket = $ticket_result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $ticket['id']; ?></td>
                    <td><?php echo htmlspecialchars($ticket['subject']); ?></td>
                    <td><?php echo nl2br(htmlspecialchars($ticket['message'])); ?></td>
                    <td><?php echo htmlspecialchars($ticket['username']); ?></td>
                    <td><?php echo htmlspecialchars($ticket['status']); ?></td>
                    <td><?php echo $ticket['created_at']; ?></td>
                    <td>
                        <?php if ($ticket['status'] === 'Open'): ?>
                            <form action="resolve_ticket.php" method="POST" style="display:inline;">
                                <input type="hidden" name="ticket_id" value="<?php echo $ticket['id']; ?>">
                                <textarea name="admin_response" rows="2" placeholder="Enter response" required></textarea>
                                <button type="submit" name="resolve_ticket">Resolve</button>
                            </form>
                        <?php else: ?>
                            <span>Resolved</span>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endwhile; ?>
        </table>
    </section>

    <p><a href="logout.php">Log Out</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
