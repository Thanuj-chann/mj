<?php
session_start();
include '../db.php';

$error_message = '';
$success_message = '';

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    // Prepare statement to fetch user details
    $stmt = $conn->prepare("SELECT id, username, password, role FROM auth_user WHERE username = ?");
    if (!$stmt) {
        die("Error preparing statement: " . $conn->error);
    }
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows === 1) {
        $user = $result->fetch_assoc();
        if (password_verify($password, $user['password'])) {
            // Set session variables
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['username'] = $user['username'];
            $_SESSION['role'] = $user['role'];
            
            // Log the login activity
            $activity_type = 'login';
            $description = "User logged in";
            $log_stmt = $conn->prepare("INSERT INTO notifications (user_id, activity_type, description) VALUES (?, ?, ?)");
            $log_stmt->bind_param("iss", $user['id'], $activity_type, $description);
            $log_stmt->execute();
            $log_stmt->close();

            // Redirect to the welcome page
            $success_message = "Login successful!";
            header("Location: welcome.php");
            exit();
        } else {
            $error_message = "Invalid username or password.";
        }
    } else {
        $error_message = "Invalid username or password.";
    }

    $stmt->close();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
</head>
<body>
    <h2>Login Form</h2>
    <?php
    if (!empty($error_message)) {
        echo "<p style='color: red;'>$error_message</p>";
    }
    if (!empty($success_message)) {
        echo "<p style='color: green;'>$success_message</p>";
    }
    ?>
    <form action="login.php" method="POST">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required><br><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br><br>

        <button type="submit" name="login">Login</button>
    </form>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
