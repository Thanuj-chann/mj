<?php
session_start();
include '../db.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$user_id = $_SESSION['user_id'];
$error_message = '';
$success_message = '';

// Fetch user profile
$stmt = $conn->prepare("SELECT bio, contact_info, profile_picture, approved FROM user_profiles WHERE user_id = ?");
$stmt->bind_param("i", $user_id);
$stmt->execute();
$profile = $stmt->get_result()->fetch_assoc();
$stmt->close();

// Profile update
if (isset($_POST['update_profile'])) {
    $bio = $_POST['bio'];
    $contact_info = $_POST['contact_info'];
    
    // Handle profile picture upload
    $profile_picture = $profile['profile_picture'];
    if (!empty($_FILES['profile_picture']['name'])) {
        $target_dir = "uploads/";
        $profile_picture = $target_dir . basename($_FILES["profile_picture"]["name"]);
        if (move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $profile_picture)) {
            $success_message = "Profile picture updated successfully.";
        } else {
            $error_message = "Error uploading profile picture.";
        }
    }

    // Update profile in database
    $stmt = $conn->prepare("REPLACE INTO user_profiles (user_id, bio, contact_info, profile_picture) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("isss", $user_id, $bio, $contact_info, $profile_picture);
    if ($stmt->execute()) {
        $success_message = "Profile updated successfully.";
    } else {
        $error_message = "Error updating profile: " . $stmt->error;
    }
    $stmt->close();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Profile</title>
</head>
<body>
    <h2>User Profile</h2>

    <?php
    if (!empty($error_message)) {
        echo "<p style='color: red;'>$error_message</p>";
    }
    if (!empty($success_message)) {
        echo "<p style='color: green;'>$success_message</p>";
    }
    ?>

    <form action="profile.php" method="POST" enctype="multipart/form-data">
        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="4" cols="50"><?php echo htmlspecialchars($profile['bio'] ?? ''); ?></textarea><br><br>

        <label for="contact_info">Contact Info:</label><br>
        <input type="text" id="contact_info" name="contact_info" value="<?php echo htmlspecialchars($profile['contact_info'] ?? ''); ?>"><br><br>

        <label for="profile_picture">Profile Picture:</label><br>
        <?php if (!empty($profile['profile_picture'])): ?>
            <img src="<?php echo htmlspecialchars($profile['profile_picture']); ?>" alt="Profile Picture" width="100"><br>
        <?php endif; ?>
        <input type="file" id="profile_picture" name="profile_picture"><br><br>

        <button type="submit" name="update_profile">Update Profile</button>
    </form>

    <p><a href="welcome.php">Back to Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
