<?php
session_start();
include '../db.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$error_message = '';
$success_message = '';

if (isset($_POST['submit_ticket'])) {
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $user_id = $_SESSION['user_id'];

    if (empty($subject) || empty($message)) {
        $error_message = "Both subject and message are required.";
    } else {
        $stmt = $conn->prepare("INSERT INTO tickets (user_id, subject, message) VALUES (?, ?, ?)");
        $stmt->bind_param("iss", $user_id, $subject, $message);

        if ($stmt->execute()) {
            $success_message = "Your support request has been submitted successfully!";
        } else {
            $error_message = "Error submitting ticket: " . $stmt->error;
        }
        $stmt->close();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submit Support Ticket</title>
</head>
<body>
    <h2>Submit a Support Ticket</h2>

    <?php
    if (!empty($error_message)) {
        echo "<p style='color: red;'>$error_message</p>";
    }
    if (!empty($success_message)) {
        echo "<p style='color: green;'>$success_message</p>";
    }
    ?>

    <form action="submit_ticket.php" method="POST">
        <label for="subject">Subject:</label><br>
        <input type="text" id="subject" name="subject" required><br><br>

        <label for="message">Message:</label><br>
        <textarea id="message" name="message" rows="4" cols="50" required></textarea><br><br>

        <button type="submit" name="submit_ticket">Submit Ticket</button>
    </form>

    <p><a href="welcome.php">Back to Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
