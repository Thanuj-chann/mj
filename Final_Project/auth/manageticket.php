<?php
session_start();
include '../db.php';

// Check if the user is logged in and is an admin
if (!isset($_SESSION['user_id']) || $_SESSION['role'] !== 'admin') {
    header("Location: login.php");
    exit();
}

if (isset($_POST['resolve_ticket'])) {
    $ticket_id = $_POST['ticket_id'];
    $response = $_POST['admin_response'];

    $stmt = $conn->prepare("UPDATE tickets SET admin_response = ?, status = 'Resolved', resolved_at = NOW() WHERE id = ?");
    $stmt->bind_param("si", $response, $ticket_id);
    if (!$stmt->execute()) {
        die("Error resolving ticket: " . $stmt->error);
    }
    $stmt->close();
}

// Fetch all tickets
$tickets = $conn->query("SELECT t.id, t.subject, t.message, t.admin_response, t.status, u.username, t.created_at 
                         FROM tickets t JOIN auth_user u ON t.user_id = u.id
                         ORDER BY t.status DESC, t.created_at DESC");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Support Tickets</title>
</head>
<body>
    <h2>Support Tickets</h2>

    <?php while ($ticket = $tickets->fetch_assoc()): ?>
        <div style="border: 1px solid #000; margin-bottom: 10px; padding: 10px;">
            <p><strong>Ticket ID:</strong> <?php echo $ticket['id']; ?></p>
            <p><strong>User:</strong> <?php echo htmlspecialchars($ticket['username']); ?></p>
            <p><strong>Subject:</strong> <?php echo htmlspecialchars($ticket['subject']); ?></p>
            <p><strong>Message:</strong> <?php echo nl2br(htmlspecialchars($ticket['message'])); ?></p>
            <p><strong>Status:</strong> <?php echo htmlspecialchars($ticket['status']); ?></p>
            <p><strong>Submitted on:</strong> <?php echo $ticket['created_at']; ?></p>

            <?php if ($ticket['status'] === 'Open'): ?>
                <form action="manageticket.php" method="POST">
                    <label for="admin_response">Response:</label><br>
                    <textarea name="admin_response" rows="3" cols="50" required></textarea><br><br>
                    <input type="hidden" name="ticket_id" value="<?php echo $ticket['id']; ?>">
                    <button type="submit" name="resolve_ticket">Resolve Ticket</button>
                </form>
            <?php else: ?>
                <p><strong>Admin Response:</strong> <?php echo nl2br(htmlspecialchars($ticket['admin_response'])); ?></p>
            <?php endif; ?>
        </div>
    <?php endwhile; ?>

    <p><a href="admin_dashboard.php">Back to Admin Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>