<?php
session_start();

// Check if user is logged in
if (!isset($_SESSION['user_id']) || !isset($_SESSION['username'])) {
    header("Location: auth/login.php");
    exit();
}

// Initialize variables with default values
$user_analytics = [
    'page_views' => 0,
    'unique_visitors' => 0,
    'average_time_on_site' => '0m 0s',
    'bounce_rate' => '0%'
];

$conversion_data = [
    'sign_ups' => 0,
    'purchases' => 0,
    'newsletter_subscriptions' => 0
];

$heatmap_data = [];

// Process form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Update user analytics
    $user_analytics['page_views'] = intval($_POST['page_views']);
    $user_analytics['unique_visitors'] = intval($_POST['unique_visitors']);
    $user_analytics['average_time_on_site'] = $_POST['average_time_on_site'];
    $user_analytics['bounce_rate'] = $_POST['bounce_rate'] . '%';

    // Update conversion data
    $conversion_data['sign_ups'] = intval($_POST['sign_ups']);
    $conversion_data['purchases'] = intval($_POST['purchases']);
    $conversion_data['newsletter_subscriptions'] = intval($_POST['newsletter_subscriptions']);

    // Update heatmap data
    for ($i = 1; $i <= 3; $i++) {
        if (!empty($_POST["heatmap_x_$i"]) && !empty($_POST["heatmap_y_$i"]) && !empty($_POST["heatmap_value_$i"])) {
            $heatmap_data[] = [
                'x' => intval($_POST["heatmap_x_$i"]),
                'y' => intval($_POST["heatmap_y_$i"]),
                'value' => intval($_POST["heatmap_value_$i"])
            ];
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Analytics and Tracking Services</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <style>
        body { font-family: Arial, sans-serif; line-height: 1.6; padding: 20px; }
        h1, h2 { color: #333; }
        .section { margin-bottom: 30px; }
        .analytics-grid { display: grid; grid-template-columns: repeat(2, 1fr); gap: 20px; }
        .analytics-item { background-color: #f4f4f4; padding: 15px; border-radius: 5px; }
        #heatmap-container { position: relative; width: 500px; height: 300px; border: 1px solid #ccc; }
        .heatmap-point { position: absolute; border-radius: 50%; opacity: 0.7; }
        form { margin-bottom: 20px; }
        label { display: inline-block; width: 200px; }
        input[type="text"], input[type="number"] { width: 100px; margin-bottom: 10px; }
    </style>
</head>
<body>
    <h1>Analytics and Tracking Services</h1>

    <form method="post">
        <h2>Enter Analytics Data</h2>
        
        <h3>User Analytics</h3>
        <div>
            <label for="page_views">Page Views:</label>
            <input type="number" id="page_views" name="page_views" value="<?= $user_analytics['page_views'] ?>">
        </div>
        <div>
            <label for="unique_visitors">Unique Visitors:</label>
            <input type="number" id="unique_visitors" name="unique_visitors" value="<?= $user_analytics['unique_visitors'] ?>">
        </div>
        <div>
            <label for="average_time_on_site">Average Time on Site:</label>
            <input type="text" id="average_time_on_site" name="average_time_on_site" value="<?= $user_analytics['average_time_on_site'] ?>">
        </div>
        <div>
            <label for="bounce_rate">Bounce Rate (%):</label>
            <input type="text" id="bounce_rate" name="bounce_rate" value="<?= rtrim($user_analytics['bounce_rate'], '%') ?>">
        </div>

        <h3>Conversion Data</h3>
        <div>
            <label for="sign_ups">Sign Ups:</label>
            <input type="number" id="sign_ups" name="sign_ups" value="<?= $conversion_data['sign_ups'] ?>">
        </div>
        <div>
            <label for="purchases">Purchases:</label>
            <input type="number" id="purchases" name="purchases" value="<?= $conversion_data['purchases'] ?>">
        </div>
        <div>
            <label for="newsletter_subscriptions">Newsletter Subscriptions:</label>
            <input type="number" id="newsletter_subscriptions" name="newsletter_subscriptions" value="<?= $conversion_data['newsletter_subscriptions'] ?>">
        </div>

        <h3>Heatmap Data (3 points)</h3>
        <?php for ($i = 1; $i <= 3; $i++): ?>
            <div>
                <label>Point <?= $i ?>:</label>
                X: <input type="number" name="heatmap_x_<?= $i ?>" value="<?= isset($heatmap_data[$i-1]) ? $heatmap_data[$i-1]['x'] : '' ?>" style="width: 50px;">
                Y: <input type="number" name="heatmap_y_<?= $i ?>" value="<?= isset($heatmap_data[$i-1]) ? $heatmap_data[$i-1]['y'] : '' ?>" style="width: 50px;">
                Value: <input type="number" name="heatmap_value_<?= $i ?>" value="<?= isset($heatmap_data[$i-1]) ? $heatmap_data[$i-1]['value'] : '' ?>" style="width: 50px;">
            </div>
        <?php endfor; ?>

        <input type="submit" value="Update Analytics">
    </form>

    <div class="section">
        <h2>User Analytics</h2>
        <div class="analytics-grid">
            <?php foreach ($user_analytics as $key => $value): ?>
                <div class="analytics-item">
                    <h3><?= ucwords(str_replace('_', ' ', $key)) ?></h3>
                    <p><?= $value ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="section">
        <h2>Conversion Tracking</h2>
        <canvas id="conversionChart"></canvas>
    </div>

    <div class="section">
        <h2>Heatmap</h2>
        <div id="heatmap-container">
            <?php foreach ($heatmap_data as $point): ?>
                <div class="heatmap-point" style="left: <?= $point['x'] ?>px; top: <?= $point['y'] ?>px; width: <?= $point['value'] ?>px; height: <?= $point['value'] ?>px; background-color: rgba(255, 0, 0, <?= $point['value'] / 100 ?>);"></div>
            <?php endforeach; ?>
        </div>
    </div>

    <script>
        // Conversion Tracking Chart
        var ctx = document.getElementById('conversionChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode(array_keys($conversion_data)) ?>,
                datasets: [{
                    label: 'Conversions',
                    data: <?= json_encode(array_values($conversion_data)) ?>,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)'
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
</body>
</html>