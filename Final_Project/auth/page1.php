<?php
session_start();

// Check if user is logged in
if (!isset($_SESSION['user_id']) || !isset($_SESSION['username'])) {
    header("Location: auth/login.php");
    exit();
}

// Include the file with our functions
include '../subscription_functions.php';

$user_id = $_SESSION['user_id'];

// Get user's invoices
$invoices = get_user_invoices($user_id);

// Handle form submissions
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['generate_invoice'])) {
        $amount = 19.99; // Example amount
        $subscription_details = "Monthly Premium Subscription";
        $invoice = generate_invoice($user_id, $amount, $subscription_details);
        if ($invoice) {
            $invoices[] = $invoice; // Add the new invoice to the list
        }
    }

    // Handle currency conversion
    if (isset($_POST['convert_currency'])) {
        $amount = floatval($_POST['amount']);
        $from_currency = $_POST['from_currency'];
        $to_currency = $_POST['to_currency'];
        $converted_amount = convert_currency($amount, $from_currency, $to_currency);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment and Subscription Services</title>
    <style>
        body { font-family: Arial, sans-serif; line-height: 1.6; padding: 20px; }
        h1, h2 { color: #333; }
        .section { margin-bottom: 30px; }
        .button { display: inline-block; padding: 10px 20px; background-color: #007BFF; color: white; text-decoration: none; border-radius: 5px; }
    </style>
</head>
<body>
    <h1>Payment and Subscription Services</h1>

    <div class="section">
    <h2>Your Invoices:</h2>
    <?php if (empty($invoices)): ?>
        <p>You have no invoices.</p>
    <?php else: ?>
        <ul>
            <?php foreach ($invoices as $invoice): ?>
                <li>
                    Invoice #<?php echo htmlspecialchars($invoice['invoice_number']); ?> - 
                    $<?php echo number_format($invoice['amount'], 2); ?> 
                    (<?php echo htmlspecialchars($invoice['status']); ?>)
                    <br>
                    Details: <?php echo isset($invoice['subscription_details']) ? htmlspecialchars($invoice['subscription_details']) : 'No details available'; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
    <div class="section">
        <h2>Generate New Invoice</h2>
        <form method="post">
            <button type="submit" name="generate_invoice" class="button">Generate Invoice</button>
        </form>
    </div>

    <div class="section">
        <h2>Currency Converter</h2>
        <form method="post">
            <label for="amount">Amount:</label>
            <input type="number" step="0.01" id="amount" name="amount" required>
            
            <label for="from_currency">From Currency:</label>
            <select id="from_currency" name="from_currency" required>
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
                <option value="GBP">GBP</option>
                <option value="JPY">JPY</option>
                <option valiue="IND">IND</option>
            </select>
            
            <label for="to_currency">To Currency:</label>
            <select id="to_currency" name="to_currency" required>
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
                <option value="GBP">GBP</option>
                <option value="JPY">JPY</option>
                <option valiue="IND">IND</option>
            </select>
            
            <button type="submit" name="convert_currency" class="button">Convert</button>
        </form>
        
        <?php if (isset($converted_amount)): ?>
            <p>
                <?php echo number_format($amount, 2) . ' ' . htmlspecialchars($from_currency); ?> = 
                <?php echo number_format($converted_amount, 2) . ' ' . htmlspecialchars($to_currency); ?>
            </p>
        <?php endif; ?>
    </div>

    <a href="auth/welcome.php" class="button">Back to Welcome Page</a>
</body>
</html>