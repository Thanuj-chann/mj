<?php
session_start();
include '../db.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

$user_id = $_SESSION['user_id'];

// Fetch user's notifications
$stmt = $conn->prepare("SELECT activity_type, description, created_at FROM notifications WHERE user_id = ? ORDER BY created_at DESC");
$stmt->bind_param("i", $user_id);
$stmt->execute();
$notifications = $stmt->get_result();
$stmt->close();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Notifications</title>
</head>
<body>
    <h2>Your Activity Notifications</h2>

    <?php if ($notifications->num_rows > 0): ?>
        <ul>
            <?php while ($notification = $notifications->fetch_assoc()): ?>
                <li>
                    <strong><?php echo htmlspecialchars($notification['activity_type']); ?></strong>: 
                    <?php echo htmlspecialchars($notification['description']); ?><br>
                    <small><?php echo $notification['created_at']; ?></small>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php else: ?>
        <p>No recent activity found.</p>
    <?php endif; ?>

    <p><a href="welcome.php">Back to Dashboard</a></p>
</body>
</html>

<?php
if (isset($conn)) {
    $conn->close();
}
?>
