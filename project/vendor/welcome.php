<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome</title>
    <style>
       body {
    background-color: grey;
    font-family: Roobert, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}

.h1 {
    text-align: center;
    font-family: serif;
}

.button-27 {
    appearance: none;
    background-color: #000000;
    border: 2px solid #1A1A1A;
    border-radius: 15px;
    box-sizing: border-box;
    color: #FFFFFF;
    cursor: pointer;
    display: inline-block;
    font-size: 16px;
    font-weight: 600;
    line-height: normal;
    margin: 0;
    min-height: 60px;
    min-width: 0;
    outline: none;
    padding: 16px 24px;
    text-align: center;
    text-decoration: none;
    transition: all 300ms cubic-bezier(0.23, 1, 0.32, 1);
    user-select: none;
    -webkit-user-select: none;
    touch-action: manipulation;
    
}

.button-27:disabled {
    pointer-events: none;
}

.button-27:hover {
    box-shadow: rgba(0, 0, 0, 0.25) 0 8px 15px;
    transform: translateY(-2px);
}

.button-27:active {
    box-shadow: none;
    transform: translateY(0);
}
    </style>
</head>
<body>
    <h1>Welcome to the Home Page, <?php echo htmlspecialchars($_SESSION['username']); ?>!</h1>
    <a href="logout.php">Logout</a>

    <h2>Select a Page:</h2>
    <div class="button-container">
        <div class="left">
            <a class="button-27" href="page1.php">User's Subscriptions and invoices</a>
            <a class="button-27" href="page2.php">Basic Analytics Service</a>
            <a class="button-27" href="page3.php">Stock Asset Information</a>
            <a class="button-27" href="page4.php">List of stock assets</a>
            <a class="button-27" href="page5.php">Market Information</a>
        </div>
        <div class="right">
            <a class="button-27" href="profile.php">Profile</a>
            <a class="button-27" href="create_post.php">Create Post</a>
            <a class="button-27" href="submit_ticket.php">Raise a Ticket</a>
            <a class="button-27" href="manageticket.php">view ticket</a>
            <a class="button-27" href="usernotification.php">User Activity</a>
            <a class="button-27" href="adminnotifications.php">Admin Notifications</a>
            <a class="button-27" href="welcom.php">User's Dashboard</a>
        </div>
    </div>
</body>
</html>